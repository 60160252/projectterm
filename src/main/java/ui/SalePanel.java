/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import dao.ProductDao;
import dao.ReceiptDao;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.ListModel;
import javax.swing.table.AbstractTableModel;
import model.Employee;
import model.Product;
import model.Receipt;
import model.ReceiptDetail;

/**
 *
 * @author Golf
 */
public class SalePanel extends javax.swing.JPanel {

    private ArrayList<Product> productList;
    private ProductTableModel model;
//    private ReceiptTableModel modelReceipt;
    private int amount=0;
    Product editedProduct;
    

    public SalePanel() {
        initComponents();
        ProductDao dao = new ProductDao();
        loadTable(dao);
        initSale();
    }
    
    public void loadTable(ProductDao dao) {
        productList = dao.getAll();
        model = new ProductTableModel(productList);
        tblProduct.setModel(model);
    }
    
    public void initSale(){
        txtAmount.setText(""+amount);
    }
     

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        tblProduct = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        btnPlus = new javax.swing.JButton();
        btnMinus = new javax.swing.JButton();
        txtAmount = new javax.swing.JTextField();
        btnAdd = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        lblOrder = new javax.swing.JLabel();
        lblOrderList = new javax.swing.JLabel();

        setMaximumSize(new java.awt.Dimension(715, 390));
        setMinimumSize(new java.awt.Dimension(715, 390));
        setPreferredSize(new java.awt.Dimension(715, 390));

        tblProduct.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblProduct);

        jPanel1.setMaximumSize(new java.awt.Dimension(210, 365));
        jPanel1.setMinimumSize(new java.awt.Dimension(210, 365));
        jPanel1.setPreferredSize(new java.awt.Dimension(210, 365));

        btnPlus.setFont(new java.awt.Font("TH Sarabun New", 1, 36)); // NOI18N
        btnPlus.setText("+");
        btnPlus.setMaximumSize(new java.awt.Dimension(50, 50));
        btnPlus.setMinimumSize(new java.awt.Dimension(50, 50));
        btnPlus.setPreferredSize(new java.awt.Dimension(50, 50));
        btnPlus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPlusActionPerformed(evt);
            }
        });

        btnMinus.setFont(new java.awt.Font("TH Sarabun New", 1, 36)); // NOI18N
        btnMinus.setText("-");
        btnMinus.setMaximumSize(new java.awt.Dimension(50, 50));
        btnMinus.setMinimumSize(new java.awt.Dimension(50, 50));
        btnMinus.setPreferredSize(new java.awt.Dimension(50, 50));
        btnMinus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMinusActionPerformed(evt);
            }
        });

        txtAmount.setFont(new java.awt.Font("TH Sarabun New", 1, 24)); // NOI18N
        txtAmount.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtAmount.setMaximumSize(new java.awt.Dimension(50, 19));
        txtAmount.setMinimumSize(new java.awt.Dimension(50, 19));

        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnDelete.setText("Delete");

        lblOrder.setBackground(new java.awt.Color(255, 255, 255));
        lblOrder.setFont(new java.awt.Font("TH Sarabun New", 0, 18)); // NOI18N
        lblOrder.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblOrder.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblOrderList, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblOrder, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(35, 35, 35)
                                .addComponent(btnAdd)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnDelete))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btnPlus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnMinus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 20, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnPlus, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnMinus, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd)
                    .addComponent(btnDelete))
                .addGap(18, 18, 18)
                .addComponent(lblOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblOrderList, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 485, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnPlusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPlusActionPerformed
        if(tblProduct.getSelectedRow()>=0){
            amount++;
            txtAmount.setText(""+amount);
            
        }
    }//GEN-LAST:event_btnPlusActionPerformed

    private void btnMinusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMinusActionPerformed
        if(amount==0)return;
        amount--;
        txtAmount.setText(""+amount);
    }//GEN-LAST:event_btnMinusActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        editedProduct = productList.get(tblProduct.getSelectedRow());
        loadProductToForm();
        
        amount = 0;
        txtAmount.setText(""+amount);
        
   
    }//GEN-LAST:event_btnAddActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnMinus;
    private javax.swing.JButton btnPlus;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblOrder;
    private javax.swing.JLabel lblOrderList;
    private javax.swing.JTable tblProduct;
    private javax.swing.JTextField txtAmount;
    // End of variables declaration//GEN-END:variables


    public void loadProductToForm(){
        if(editedProduct.getId()>=0)
            lblOrder.setText(""+editedProduct.getName()+" "+ amount+" "+editedProduct.getPrice()*amount);
//        lstOrder
        Employee emp = new Employee(1, "", "");
        Product p = new Product(editedProduct.getId(), editedProduct.getName(), editedProduct.getPrice());
        Receipt receipt = new Receipt(emp);
        receipt.addReceiptDetail(p, amount);
        System.out.println(receipt);
        ReceiptDao dao = new ReceiptDao();
        System.out.println("id = " + dao.add(receipt));
        
        
        
    }
    private class ProductTableModel extends AbstractTableModel {

        private final ArrayList<Product> data;
        String[] columnName = {"ID", "Name", "Price"};

        public ProductTableModel(ArrayList<Product> data) {
            this.data = data;
        }

        @Override
        public int getRowCount() {
            return this.data.size();
        }

        @Override
        public int getColumnCount() {
            return 3;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Product product = this.data.get(rowIndex);
            if (columnIndex == 0) {
                return product.getId();
            }
            if (columnIndex == 1) {
                return product.getName();
            }
            if (columnIndex == 2) {
                return product.getPrice();
            }
            return "";
        }

        @Override
        public String getColumnName(int column) {
            return columnName[column];
        }

    }
//    private class ReceiptTableModel extends AbstractTableModel {
//
//        private final ArrayList<ReceiptDetail> data;
//        String[] columnName = {"ID", "Name", "Price"};
//
//        public ReceiptTableModel(ArrayList<ReceiptDetail> data) {
//            this.data = data;
//        }
//
//        @Override
//        public int getRowCount() {
//            return this.data.size();
//        }
//
//        @Override
//        public int getColumnCount() {
//            return 3;
//        }
//
//        @Override
//        public Object getValueAt(int rowIndex, int columnIndex) {
//            ReceiptDetail receipt = this.data.get(rowIndex);
//            if (columnIndex == 0) {
//                return receipt.getId();
//            }
//            if (columnIndex == 1) {
//                return receipt.getProduct();
//            }
//            if (columnIndex == 2) {
//                return receipt.getPrice();
//            }
//            return "";
//        }
//
//        @Override
//        public String getColumnName(int column) {
//            return columnName[column];
//        }
//
//    }

}
