/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author HP
 */
public class Employee {

    private int id;
    private String Firstname;
    private String Lastname;
    private String Username;
    private String Password;
    private int Age;
    private String Gender;
    private String Tel;
    private Double Salary;
    private String DateStartjob;

    public Employee(int id, String Firstname, String Lastname, String Username, String Password, int Age, String Gender, String Tel, Double Salary, String DateStartjob) {
        this.id = id;
        this.Firstname = Firstname;
        this.Lastname = Lastname;
        this.Username = Username;
        this.Password = Password;
        this.Age = Age;
        this.Gender = Gender;
        this.Tel = Tel;
        this.Salary = Salary;
        this.DateStartjob = DateStartjob;
    }

    public Employee(String Firstname, String Lastname, String Username, String Password, int Age, String Gender, String Tel, Double Salary, String DateStartjob) {
        this(-1, Firstname, Lastname, Username, Password, Age, Gender, Tel, Salary, DateStartjob);
    }

    public Employee(int id, String Firstname, String Tel) {
        this.id = id;
        this.Firstname = Firstname;
        this.Tel = Tel;
    }

    
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return Firstname;
    }

    public void setFirstname(String Firstname) {
        this.Firstname = Firstname;
    }

    public String getLastname() {
        return Lastname;
    }

    public void setLastname(String Lastname) {
        this.Lastname = Lastname;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int Age) {
        this.Age = Age;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    public String getTel() {
        return Tel;
    }

    public void setTel(String Tel) {
        this.Tel = Tel;
    }

    public double getSalary() {
        return Salary;
    }

    public void setSalary(double Salary) {
        this.Salary = Salary;
    }

    public String getDateStartjob() {
        return DateStartjob;
    }

    public void setDateStartjob(String DateStartjob) {
        this.DateStartjob = DateStartjob;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", Firstname=" + Firstname + ", Lastname=" + Lastname + ", Username=" + Username + ", "
                + "Password=" + Password + ", Age=" + Age + ", Gender=" + Gender + ", Tel=" + Tel + ", Salary=" + Salary + ", "
                + "DateStartjob=" + DateStartjob + '}';
    }

}
