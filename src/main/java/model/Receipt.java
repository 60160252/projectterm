/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;


import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Lenovo
 */
public class Receipt {

    private int id;
    private Date created;
    private Employee employee;
//    private Customer customer;
    private ArrayList<ReceiptDetail> receiptDetail;

    public Receipt(int id, Date created, Employee employee) {
        this.id = id;
        this.created = created;
        this.employee = employee;
//        this.customer = customer;
        receiptDetail = new ArrayList<>();
    }

    public Receipt(Employee employee) {
        this(-1, null, employee);
    }
    
    public void addReceiptDetail(int id,Product product, int amount ,double price) {
       for(int row = 0;row<receiptDetail.size();row++){
           ReceiptDetail r = receiptDetail.get(row);
           if(r.getProduct().getId()== product.getId()){
               r.addAmount(amount);
               return;
           }
       }
       receiptDetail.add(new ReceiptDetail(id,product, amount, price, this));
    }  
    
    public void addReceiptDetail(Product product, int amount) {
        addReceiptDetail(-1,product,amount,product.getPrice());
    }
    
        public void deleteReceiptDetail(int row){
        receiptDetail.remove(row);
    }
    
    public double getTotal(){
        double total = 0;
        for(ReceiptDetail r:receiptDetail){
            total = total + r.getTotal();
        }
         return total;
    }
    
    public int getId() {
        return id;
    }

    public Date getCreated() {
        return created;
    }

    public Employee getEmployee() {
        return employee;
    }

//    public Customer getCustomer() {
//        return customer;
//    }

    public ArrayList<ReceiptDetail> getReceiptDetail() {
        return receiptDetail;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

//    public void setCustomer(Customer customer) {
//        this.customer = customer;
//    }

    public void setReceiptDetail(ArrayList<ReceiptDetail> receiptDetail) {
        this.receiptDetail = receiptDetail;
    }

    @Override
    public String toString() {
        String str =  "Receipt{" + "id=" + id 
                + ", created=" + created 
                + ", Employee=" + employee 
                + ", total=" + this.getTotal()
                + "}\n";
        for(ReceiptDetail r: receiptDetail){
            str = str + r.toString()+ "\n";
        }
        return str;
    }

}
