/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.projectterm;

import model.Customer;
import model.Employee;
import model.Product;
import model.Receipt;

/**
 *
 * @author Lenovo
 */
public class TestReceipt {

    public static void main(String[] args) {
        Product p1 = new Product(1, "Green tea", 40);
        Product p2 = new Product(2, "Americano", 50);
        Employee employee = new Employee("Natcha", "Somjit", "Natch a", "Dear2240", 21, "Female", "954850194", 30000.0, "03-05-2020");
//        Customer customer = new Customer("onchaporn", "somji", "0825544556", 100);
        Receipt receipt = new Receipt(employee);
        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 3);
        System.out.println(receipt);
        receipt.deleteReceiptDetail(0);
        System.out.println(receipt);
        receipt.addReceiptDetail(p1, 2);
        System.out.println(receipt);
        receipt.addReceiptDetail(p1, 2);
        System.out.println(receipt);
    }
}
