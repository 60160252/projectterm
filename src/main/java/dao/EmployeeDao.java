/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Employee;

/**
 *
 * @author Golf
 */
public class EmployeeDao {

    public int add(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO EMPLOYEE ( EMP_FIRSTNAME,EMP_LASTNAME,EMP_USERNAME,\n"
                    + "                         EMP_PASSWORD, EMP_AGE, EMP_GENDER,EMP_TEL,EMP_SALARY,\n"
                    + "                         EMP_DATESTARTJOB )\n"
                    + "                     VALUES (?,?,?,?,?,?,?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getFirstname());
            stmt.setString(2, object.getLastname());
            stmt.setString(3, object.getUsername());
            stmt.setString(4, object.getPassword());
            stmt.setInt(5, object.getAge());
            stmt.setString(6, object.getGender());
            stmt.setString(7, object.getTel());
            stmt.setDouble(8, object.getSalary());
            stmt.setString(9, object.getDateStartjob());

            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("Error: SQL error");
        }

        db.close();
        return id;
    }

    public ArrayList<Employee> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT EMP_ID,\n"
                    + "       EMP_FIRSTNAME,\n"
                    + "       EMP_LASTNAME,\n"
                    + "       EMP_USERNAME,\n"
                    + "       EMP_PASSWORD,\n"
                    + "       EMP_AGE,\n"
                    + "       EMP_GENDER,\n"
                    + "       EMP_TEL,\n"
                    + "       EMP_SALARY,\n"
                    + "       EMP_DATESTARTJOB\n"
                    + "  FROM EMPLOYEE;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("EMP_ID");
                String Firstname = result.getString("EMP_FIRSTNAME");
                String Lastname = result.getString("EMP_LASTNAME");
                String Username = result.getString("EMP_USERNAME");
                String Password = result.getString("EMP_PASSWORD");
                int Age = result.getInt("EMP_AGE");
                String Gender = result.getString("EMP_GENDER");
                String Tel = result.getString("EMP_TEL");
                Double Salary = result.getDouble("EMP_SALARY");
                String DateStartjob = result.getString("EMP_DATESTARTJOB");
                Employee employee = new Employee(id, Firstname, Lastname, Username, Password, Age, Gender, Tel, Salary, DateStartjob);
                list.add(employee);
            }
        } catch (SQLException ex) {
            System.out.println("Error: SQL error" + ex.getMessage());
        }

        db.close();
        return list;
    }

    public Employee get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT EMP_ID,\n"
                    + "       EMP_FIRSTNAME,\n"
                    + "       EMP_LASTNAME,\n"
                    + "       EMP_USERNAME,\n"
                    + "       EMP_PASSWORD,\n"
                    + "       EMP_AGE,\n"
                    + "       EMP_GENDER,\n"
                    + "       EMP_TEL,\n"
                    + "       EMP_SALARY,\n"
                    + "       EMP_DATESTARTJOB\n"
                    + "  FROM EMPLOYEE"
                    + " WHERE EMP_ID=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                String Firstname = result.getString("EMP_FIRSTNAME");
                String Lastname = result.getString("EMP_LASTNAME");
                String Username = result.getString("EMP_USERNAME");
                String Password = result.getString("EMP_PASSWORD");
                int Age = result.getInt("EMP_AGE");
                String Gender = result.getString("EMP_GENDER");
                String Tel = result.getString("EMP_TEL");
                Double Salary = result.getDouble("EMP_SALARY");
                String DateStartjob = result.getString("EMP_DATESTARTJOB");
                Employee employee = new Employee(id, Firstname, Lastname, Username, Password, Age, Gender, Tel, Salary, DateStartjob);
                return employee;
            }
        } catch (SQLException ex) {
            System.out.println("Error: SQL error");
        }
        return null;
    }

    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM EMPLOYEE WHERE EMP_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: SQL error");
        }

        db.close();
        return row;
    }

    public int update(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE EMPLOYEE\n"
                    + "   SET EMP_ID = ?,\n"
                    + "       EMP_FIRSTNAME = ?,\n"
                    + "       EMP_LASTNAME = ?,\n"
                    + "       EMP_USERNAME = ?,\n"
                    + "       EMP_PASSWORD = ?,\n"
                    + "       EMP_AGE = ?,\n"
                    + "       EMP_GENDER = ?,\n"
                    + "       EMP_TEL = ?,\n"
                    + "       EMP_SALARY = ?,\n"
                    + "       EMP_DATESTARTJOB = ?\n"
                    + " WHERE EMP_ID = ?";

            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getFirstname());
            stmt.setString(2, object.getLastname());
            stmt.setString(3, object.getUsername());
            stmt.setString(4, object.getPassword());
            stmt.setInt(5, object.getAge());
            stmt.setString(6, object.getGender());
            stmt.setString(7, object.getTel());
            stmt.setDouble(8, object.getSalary());
            stmt.setString(9, object.getDateStartjob());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: SQL error");
        }

        db.close();
        return row;
    }

    public static void main(String args[]) {
        EmployeeDao dao = new EmployeeDao();
        System.out.println(dao.getAll());

        System.out.println(dao.get(1));

        int id = dao.add(new Employee(-1, "Piyawan", "Jiemhuadlee", "Piyawan", "Tang2240", 21, "Fenmale", "0954850194", 30000.00, "03-05-2020"));
        System.out.println("id: " + id);
        
        Employee lastEmployee = dao.get(id);
        System.out.println("last Employee: "+ lastEmployee);
        lastEmployee.setAge(60);
        int row = dao.update(lastEmployee);

        Employee updateEmployee = dao.get(id);
        System.out.println("update Employee: "+ updateEmployee);
        
        dao.delete(id);
        Employee deleteEmployee = dao.get(id);
        System.out.println("delete Employee: " + deleteEmployee);
    }
    }

