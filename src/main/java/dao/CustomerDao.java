/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import static java.nio.file.Files.list;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Product;
/**
 *
 * @author 
 */
public class CustomerDao implements DaoInterface<Customer> {

    @Override
    public int add(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;

        try {
            String sql = "INSERT INTO CUSTOMER (CUS_NAME, CUS_SURNAME, CUS_TEL, CUS_POINT)VALUES (?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getSurName());
            stmt.setString(3, object.getPhone());
            stmt.setInt(4, object.getPoint());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR: SQL error1");
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT CUS_ID, CUS_NAME, CUS_SURNAME, CUS_TEL, CUS_POINT FROM CUSTOMER";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("CUS_ID");
                String name = result.getString("CUS_NAME");
                String surname = result.getString("CUS_SURNAME");
                String phone = result.getString("CUS_TEL");
                int point = result.getInt("CUS_POINT");
                Customer customer = new Customer(id, name, surname, phone, point);
                list.add(customer);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR: SQL error2");
        }
        db.close();
        return list;
    }

    @Override
    public Customer get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT CUS_ID, CUS_NAME, CUS_SURNAME, CUS_TEL, CUS_POINT FROM CUSTOMER";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int cus_id = result.getInt("CUS_ID");
                String name = result.getString("CUS_NAME");
                String surname = result.getString("CUS_SURNAME");
                String phone = result.getString("CUS_TEL");
                int point = result.getInt("CUS_POINT");
                Customer customer = new Customer(cus_id, name, surname, phone, point);
                return customer;

            }
        } catch (SQLException ex) {
            System.out.println("ERROR: SQL error3");
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM CUSTOMER WHERE CUS_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("ERROR: SQL error4");
        }

        db.close();
        return row;
    }

    @Override
    public int update(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE CUSTOMER SET CUS_NAME = ?, CUS_SURNAME = ?, CUS_TEL = ?, CUS_POINT = ? WHERE CUS_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getSurName());    
            stmt.setString(3, object.getPhone());
            stmt.setInt(4, object.getPoint());
            stmt.setInt(5, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR: SQL error5");
        }
        db.close();
        return row;
    }
}

//    public static void main(String args[]) {
//        CustomerDao dao = new CustomerDao();
//        System.out.println(dao.getAll());
//        
//        System.out.println(dao.get(1));
//        
//        int id=dao.add(new Customer(-1, "Phongsathorn", "phanthasen", "0922306799", 40));
//        System.out.println("id: "+id);
//        
//        Customer lastCustomer = dao.get(id);
//        System.out.println("last customer: "+ lastCustomer);
//        lastCustomer.setPoint(60);
//        int row = dao.update(lastCustomer);
//
//        Customer updateCustomer = dao.get(id);
//        System.out.println("update customer: "+ updateCustomer);
//        
//        dao.delete(id);
//        Customer deleteCustomer = dao.get(id);
//        System.out.println("delete Customer: " + deleteCustomer);
//    }
