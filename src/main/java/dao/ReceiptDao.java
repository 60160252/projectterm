/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Employee;
import model.Product;
import model.Receipt;
import model.ReceiptDetail;

/**
 *
 * @author Lenovo
 */
public class ReceiptDao implements DaoInterface<Receipt> {

    @Override
    public int add(Receipt object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO RECEIPT (EMP_ID,REC_TOTAL)VALUES (?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setInt(1, object.getCustomer().getId());
            stmt.setInt(1, object.getEmployee().getId());
            stmt.setDouble(2, object.getTotal());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }
            for (ReceiptDetail r : object.getReceiptDetail()) {
                String sqlDetail = "INSERT INTO RECEIPT_DETAIL (REC_ID,PD_ID,DETAIL_REC_PRICE,DETAIL_REC_AMOUNT)\n" + "VALUES (?,?,?,?);";
                PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
                stmtDetail.setInt(1, r.getReceipt().getId());
                stmtDetail.setInt(2, r.getProduct().getId());
                stmtDetail.setDouble(3, r.getPrice());
                stmtDetail.setInt(4, r.getAmount());
                int rowDetail = stmtDetail.executeUpdate();
                ResultSet resultDetail = stmtDetail.getGeneratedKeys();
                if (resultDetail.next()) {
                    id = resultDetail.getInt(1);
                    r.setId(id);
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error: to create receipt" + ex.getMessage());
        }

        db.close();
        return id;
    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT r.REC_ID as REC_ID,\n"
                    + "       REC_CREATED,\n"
                    + "       r.EMP_ID as EMP_ID,\n"
                    + "       e.EMP_FIRSTNAME as EMP_FIRSTNAME,\n"
                    + "       e.EMP_TEL as EMP_TEL,\n"
                    + "       REC_TOTAL       \n"
                    + "  FROM RECEIPT r ,EMPLOYEE e\n"
                    + "  WHERE r.EMP_ID = e.EMP_ID"
                    + "  ORDER BY REC_CREATED DESC;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("REC_ID");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("REC_CREATED"));
//                int customerId = result.getInt("CUS_ID");
//                String customerName = result.getString("CUS_NAME");
//                String customerTel = result.getString("CUS_TEL");
                int employeeId = result.getInt("EMP_ID");
                String employeeName = result.getString("EMP_FIRSTNAME");
                String employeeTel = result.getString("EMP_TEL");
                double total = result.getDouble("REC_TOTAL");
                Receipt receipt = new Receipt(id, created,
                        new Employee(employeeId, employeeName, employeeTel)
                );
                list.add(receipt);
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select all receipt" + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error: Date paring all receipt" + ex.getMessage());
        }

        db.close();
        return list;
    }

    @Override
    public Receipt get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT r.REC_ID as REC_ID,\n"
                    + "       REC_CREATED,\n"
//                    + "       r.CUS_ID as CUS_ID,\n"
//                    + "       c.CUS_NAME as CUS_NAME,\n"
//                    + "       c.CUS_TEL as CUS_TEL,\n"
                    + "       r.EMP_ID as EMP_ID,\n"
                    + "       e.EMP_FIRSTNAME as EMP_FIRSTNAME,\n"
                    + "       e.EMP_TEL as EMP_TEL,\n"
                    + "       REC_TOTAL       \n"
                    + "  FROM RECEIPT r ,EMPLOYEE e\n"
                    + "  WHERE r.REC_ID = ? AND r.EMP_ID = e.EMP_ID;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                int rid = result.getInt("REC_ID");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("REC_CREATED"));
//                int customerId = result.getInt("CUS_ID");
//                String customerName = result.getString("CUS_NAME");
//                String customerTel = result.getString("CUS_TEL");
                int employeeId = result.getInt("EMP_ID");
                String employeeName = result.getString("EMP_FIRSTNAME");
                String employeeTel = result.getString("EMP_TEL");
                double total = result.getDouble("REC_TOTAL");
                Receipt receipt = new Receipt(rid, created,
                        new Employee(employeeId, employeeName, employeeTel)
//                        new Customer(customerId, customerName, customerTel)
                );

                getReceiptDetail(conn, id, receipt);
//                conn.commit();

                return receipt;
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select receipt id" + id + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error: Date paring all receipt" + ex.getMessage());
        }
        return null;
    }

    private void getReceiptDetail(Connection conn, int id, Receipt receipt) throws SQLException {
        String sqlDetail = "SELECT rd.REC_ID as REC_ID,\n"
                + "       p.PD_ID as PD_ID,\n"
                + "       p.PD_NAME as PD_NAME,\n"
                + "       p.PD_PRICE as PD_PRICE,\n"
                + "       rd.DETAIL_REC_PRICE as DETAIL_REC_PRICE,\n"
                + "       DETAIL_REC_AMOUNT\n"
                + "  FROM RECEIPT_DETAIL rd ,PRODUCT p\n"
                + "  WHERE REC_ID = ? AND rd.PD_ID = p.PD_ID;";
        PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
        stmtDetail.setInt(1, id);
        ResultSet resultDetail = stmtDetail.executeQuery();
        while (resultDetail.next()) {
            int receiveId = resultDetail.getInt("REC_ID");
            int productId = resultDetail.getInt("PD_ID");
            String productName = resultDetail.getString("PD_NAME");
            double productPrice = resultDetail.getDouble("PD_PRICE");
            double price = resultDetail.getDouble("DETAIL_REC_PRICE");
            int amount = resultDetail.getInt("DETAIL_REC_AMOUNT");
            Product product = new Product(productId, productName, productPrice);
            receipt.addReceiptDetail(receiveId, product, amount, price);
        }
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM RECEIPT WHERE REC_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: Unable to delete receipt id" + id);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Receipt object) {
        //        Connection conn = null;
//        Database db = Database.getInstance();
//        conn = db.getConnection();
//        int row = 0;
//        try {
//            String sql = "UPDATE PRODUCT SET PD_NAME = ?,PD_PRICE = ? WHERE PD_ID = ?";
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, object.getName());
//            stmt.setDouble(2, object.getPrice());
//            stmt.setInt(3, object.getId());
//            row = stmt.executeUpdate();
//        } catch (SQLException ex) {
//            System.out.println("Error: SQL error");
//        }
//
//        db.close();
        return 0;
    }

    public static void main(String[] args) {
        Product p1 = new Product(1, "Americano", 40);
        Product p2 = new Product(2, "Green tea", 30);
        Employee employee = new Employee(1, "Piyawan", "954850194");
//        Customer customer = new Customer(1, "onchaporn", "0825544556");
        Receipt receipt = new Receipt(employee);
        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 3);
        System.out.println(receipt);
        ReceiptDao dao = new ReceiptDao();
        System.out.println("id = " + dao.add(receipt));
        System.out.println("Receipt after add " + receipt);
        System.out.println("Get all " + dao.getAll());
        Receipt newReceipt = dao.get(receipt.getId());
        System.out.println("New receipt: " + newReceipt);

        dao.delete(receipt.getId());
    }

}
